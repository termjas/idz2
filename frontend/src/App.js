import logo from './logo.svg';
import './App.css';
const axios = require('axios');

// const makeApiRequest = () => {
//   axios.get("/api/users").then((response) => {
//     console.log(response)
//   })
// }

const makeApiRequest = () => {
  fetch("http://websystem-docker.com/api/users")
  .then(response => response.json())
  .then(data => console.log(data))
  .catch((error) => {
    console.error('Error:', error);
  });
}


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload new new new123.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button onClick={makeApiRequest}>OK</button>
      </header>
    </div>
  );
}

export default App;
