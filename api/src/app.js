const {HOST, PORT} = require('./configuration/index.js')
const {ConnectDB} = require('./utils/db.js')
const express = require("express")
const {User} = require('./models/user.js')
const app = express()

app.get('/', (req, res) => {
    res.send("Hello");
})

app.get('/users', async (req, res) => {
    try {
        const user = new User({userName:"My name is Andrew"})
        await user.save()
        const users = await User.find()
        res.json({users})
    } catch(err) {
        res.send({err})
    }
})

const startServer = ()=>{
    app.listen(PORT, ()=> {
        console.log(`Server is running on ${HOST}:${PORT}`)
    })
}

ConnectDB()
.on('error', console.error.bind(console,"connection error:"))
.once("open", startServer)