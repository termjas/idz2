module.exports = {
    HOST: process.env.HOST,
    PORT: process.env.PORT,
    DB: process.env.MONGO_URL
  };
  