const mongoose = require('mongoose')
const {DB} = require('./../configuration/index')

module.exports = {
    'ConnectDB': ()=>{
        mongoose.connect(DB, {useNewUrlParser: true});
        return mongoose.connection;
    }
}
